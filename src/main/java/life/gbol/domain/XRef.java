package life.gbol.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface XRef extends Qualifier {
  Database getDb();

  void setDb(Database val);

  void remSecondaryAccession(String val);

  List<? extends String> getAllSecondaryAccession();

  void addSecondaryAccession(String val);

  String getAccession();

  void setAccession(String val);
}
