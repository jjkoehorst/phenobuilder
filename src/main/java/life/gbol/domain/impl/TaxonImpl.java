package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Taxon;
import nl.systemsbiology.pheno.domain.Rank;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class TaxonImpl extends OWLThingImpl implements Taxon {
  public static final String TypeIRI = "http://gbol.life/0.1/Taxon";

  protected TaxonImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Taxon make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new TaxonImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Taxon.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Taxon.class,false);
          if(toRet == null) {
            toRet = new TaxonImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Taxon)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.TaxonImpl expected");
        }
      }
      return (Taxon)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://systemsbiology.nl/pheno/taxonRank");
    this.checkCardMin1("http://systemsbiology.nl/pheno/taxonName");
  }

  public Taxon getSubClassOf() {
    return this.getRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",true,Taxon.class);
  }

  public void setSubClassOf(Taxon val) {
    this.setRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val,Taxon.class);
  }

  public Rank getTaxonRank() {
    return this.getEnum("http://systemsbiology.nl/pheno/taxonRank",false,Rank.class);
  }

  public void setTaxonRank(Rank val) {
    this.setEnum("http://systemsbiology.nl/pheno/taxonRank",val,Rank.class);
  }

  public String getTaxonName() {
    return this.getStringLit("http://systemsbiology.nl/pheno/taxonName",false);
  }

  public void setTaxonName(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/taxonName",val);
  }
}
