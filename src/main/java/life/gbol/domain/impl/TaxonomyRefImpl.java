package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Database;
import life.gbol.domain.TaxonomyRef;
import nl.systemsbiology.pheno.domain.NomenclaturalType;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class TaxonomyRefImpl extends XRefImpl implements TaxonomyRef {
  public static final String TypeIRI = "http://gbol.life/0.1/TaxonomyRef";

  protected TaxonomyRefImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static TaxonomyRef make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new TaxonomyRefImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,TaxonomyRef.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,TaxonomyRef.class,false);
          if(toRet == null) {
            toRet = new TaxonomyRefImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof TaxonomyRef)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.TaxonomyRefImpl expected");
        }
      }
      return (TaxonomyRef)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/db");
    this.checkCardMin1("http://gbol.life/0.1/accession");
  }

  public Database getDb() {
    return this.getRef("http://gbol.life/0.1/db",false,Database.class);
  }

  public void setDb(Database val) {
    this.setRef("http://gbol.life/0.1/db",val,Database.class);
  }

  public void remSecondaryAccession(String val) {
    this.remStringLit("http://gbol.life/0.1/secondaryAccession",val,true);
  }

  public List<? extends String> getAllSecondaryAccession() {
    return this.getStringLitSet("http://gbol.life/0.1/secondaryAccession",true);
  }

  public void addSecondaryAccession(String val) {
    this.addStringLit("http://gbol.life/0.1/secondaryAccession",val);
  }

  public String getAccession() {
    return this.getStringLit("http://gbol.life/0.1/accession",false);
  }

  public void setAccession(String val) {
    this.setStringLit("http://gbol.life/0.1/accession",val);
  }

  public NomenclaturalType getNomenclaturalType() {
    return this.getEnum("http://systemsbiology.nl/pheno/nomenclaturalType",true,NomenclaturalType.class);
  }

  public void setNomenclaturalType(NomenclaturalType val) {
    this.setEnum("http://systemsbiology.nl/pheno/nomenclaturalType",val,NomenclaturalType.class);
  }
}
