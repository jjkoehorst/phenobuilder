package life.gbol.domain.impl;

import java.lang.String;
import java.util.List;
import life.gbol.domain.Database;
import life.gbol.domain.XRef;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class XRefImpl extends QualifierImpl implements XRef {
  public static final String TypeIRI = "http://gbol.life/0.1/XRef";

  protected XRefImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static XRef make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new XRefImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,XRef.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,XRef.class,false);
          if(toRet == null) {
            toRet = new XRefImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof XRef)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.XRefImpl expected");
        }
      }
      return (XRef)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://gbol.life/0.1/db");
    this.checkCardMin1("http://gbol.life/0.1/accession");
  }

  public Database getDb() {
    return this.getRef("http://gbol.life/0.1/db",false,Database.class);
  }

  public void setDb(Database val) {
    this.setRef("http://gbol.life/0.1/db",val,Database.class);
  }

  public void remSecondaryAccession(String val) {
    this.remStringLit("http://gbol.life/0.1/secondaryAccession",val,true);
  }

  public List<? extends String> getAllSecondaryAccession() {
    return this.getStringLitSet("http://gbol.life/0.1/secondaryAccession",true);
  }

  public void addSecondaryAccession(String val) {
    this.addStringLit("http://gbol.life/0.1/secondaryAccession",val);
  }

  public String getAccession() {
    return this.getStringLit("http://gbol.life/0.1/accession",false);
  }

  public void setAccession(String val) {
    this.setStringLit("http://gbol.life/0.1/accession",val);
  }
}
