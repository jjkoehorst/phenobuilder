package life.gbol.domain.impl;

import java.lang.String;
import life.gbol.domain.Qualifier;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class QualifierImpl extends OWLThingImpl implements Qualifier {
  public static final String TypeIRI = "http://gbol.life/0.1/Qualifier";

  protected QualifierImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Qualifier make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new QualifierImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Qualifier.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Qualifier.class,false);
          if(toRet == null) {
            toRet = new QualifierImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Qualifier)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.QualifierImpl expected");
        }
      }
      return (Qualifier)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
