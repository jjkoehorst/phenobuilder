package life.gbol.domain.impl;

import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import life.gbol.domain.Database;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.rdfs.ns._void.domain.impl.DatasetImpl;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public class DatabaseImpl extends DatasetImpl implements Database {
  public static final String TypeIRI = "http://gbol.life/0.1/Database";

  protected DatabaseImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Database make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new DatabaseImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Database.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Database.class,false);
          if(toRet == null) {
            toRet = new DatabaseImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Database)) {
          throw new RuntimeException("Instance of life.gbol.domain.impl.DatabaseImpl expected");
        }
      }
      return (Database)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://systemsbiology.nl/pheno/id");
    this.checkCardMin1("dc:hasVersion");
  }

  public String getId() {
    return this.getStringLit("http://systemsbiology.nl/pheno/id",false);
  }

  public void setId(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/id",val);
  }

  public String getDescription() {
    return this.getStringLit("dc:description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("dc:description",val);
  }

  public String getDataDump() {
    return this.getExternalRef("void:dataDump",true);
  }

  public void setDataDump(String val) {
    this.setExternalRef("void:dataDump",val);
  }

  public String getWaiver() {
    return this.getStringLit("wv:waiver",true);
  }

  public void setWaiver(String val) {
    this.setStringLit("wv:waiver",val);
  }

  public void remLanguage(String val) {
    this.remStringLit("dc:language",val,true);
  }

  public List<? extends String> getAllLanguage() {
    return this.getStringLitSet("dc:language",true);
  }

  public void addLanguage(String val) {
    this.addStringLit("dc:language",val);
  }

  public String getUriRegexPattern() {
    return this.getStringLit("void:uriRegexPattern",true);
  }

  public void setUriRegexPattern(String val) {
    this.setStringLit("void:uriRegexPattern",val);
  }

  public String getUriLookupEndpoint() {
    return this.getExternalRef("void:uriLookupEndpoint",true);
  }

  public void setUriLookupEndpoint(String val) {
    this.setExternalRef("void:uriLookupEndpoint",val);
  }

  public LocalDateTime getReleaseDate() {
    return this.getDateTimeLit("http://systemsbiology.nl/pheno/releaseDate",true);
  }

  public void setReleaseDate(LocalDateTime val) {
    this.setDateTimeLit("http://systemsbiology.nl/pheno/releaseDate",val);
  }

  public String getSparqlEndpoint() {
    return this.getExternalRef("void:sparqlEndpoint",true);
  }

  public void setSparqlEndpoint(String val) {
    this.setExternalRef("void:sparqlEndpoint",val);
  }

  public String getUriSpace() {
    return this.getExternalRef("void:uriSpace",true);
  }

  public void setUriSpace(String val) {
    this.setExternalRef("void:uriSpace",val);
  }

  public String getHasVersion() {
    return this.getStringLit("dc:hasVersion",false);
  }

  public void setHasVersion(String val) {
    this.setStringLit("dc:hasVersion",val);
  }

  public String getVersion() {
    return this.getStringLit("http://systemsbiology.nl/pheno/version",true);
  }

  public void setVersion(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/version",val);
  }

  public String getTitle() {
    return this.getStringLit("dc:title",true);
  }

  public void setTitle(String val) {
    this.setStringLit("dc:title",val);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }

  public String getLabel() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#label",val);
  }

  public String getNorms() {
    return this.getExternalRef("wv:norms",true);
  }

  public void setNorms(String val) {
    this.setExternalRef("wv:norms",val);
  }

  public String getLicense() {
    return this.getExternalRef("dc:license",true);
  }

  public void setLicense(String val) {
    this.setExternalRef("dc:license",val);
  }
}
