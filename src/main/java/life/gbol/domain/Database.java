package life.gbol.domain;

import java.lang.String;
import java.time.LocalDateTime;
import java.util.List;
import org.rdfs.ns._void.domain.Dataset;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Database extends Dataset {
  String getId();

  void setId(String val);

  String getDescription();

  void setDescription(String val);

  String getDataDump();

  void setDataDump(String val);

  String getWaiver();

  void setWaiver(String val);

  void remLanguage(String val);

  List<? extends String> getAllLanguage();

  void addLanguage(String val);

  String getUriRegexPattern();

  void setUriRegexPattern(String val);

  String getUriLookupEndpoint();

  void setUriLookupEndpoint(String val);

  LocalDateTime getReleaseDate();

  void setReleaseDate(LocalDateTime val);

  String getSparqlEndpoint();

  void setSparqlEndpoint(String val);

  String getUriSpace();

  void setUriSpace(String val);

  String getHasVersion();

  void setHasVersion(String val);

  String getVersion();

  void setVersion(String val);

  String getTitle();

  void setTitle(String val);

  String getComment();

  void setComment(String val);

  String getLabel();

  void setLabel(String val);

  String getNorms();

  void setNorms(String val);

  String getLicense();

  void setLicense(String val);
}
