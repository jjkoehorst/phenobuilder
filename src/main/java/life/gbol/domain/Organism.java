package life.gbol.domain;

import java.lang.String;
import java.util.List;
import nl.systemsbiology.pheno.domain.PhenotypeInformation;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface Organism extends OWLThing {
  void remTaxonomyLineage(Taxon val);

  List<? extends Taxon> getAllTaxonomyLineage();

  void addTaxonomyLineage(Taxon val);

  String getAlternativeNames();

  void setAlternativeNames(String val);

  TaxonomyRef getTaxonomy();

  void setTaxonomy(TaxonomyRef val);

  void remPhenotypeInformation(PhenotypeInformation val);

  List<? extends PhenotypeInformation> getAllPhenotypeInformation();

  void addPhenotypeInformation(PhenotypeInformation val);

  String getCommonName();

  void setCommonName(String val);

  String getTypeStrain();

  void setTypeStrain(String val);

  String getScientificName();

  void setScientificName(String val);
}
