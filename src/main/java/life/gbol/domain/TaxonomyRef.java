package life.gbol.domain;

import java.lang.String;
import java.util.List;
import nl.systemsbiology.pheno.domain.NomenclaturalType;

/**
 * Code generated from http://gbol.life/0.1/ ontology
 */
public interface TaxonomyRef extends XRef {
  Database getDb();

  void setDb(Database val);

  void remSecondaryAccession(String val);

  List<? extends String> getAllSecondaryAccession();

  void addSecondaryAccession(String val);

  String getAccession();

  void setAccession(String val);

  NomenclaturalType getNomenclaturalType();

  void setNomenclaturalType(NomenclaturalType val);
}
