package org.rdfs.ns._void.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.rdfs.ns._void.domain.Dataset;

/**
 * Code generated from http://rdfs.org/ns/void# ontology
 */
public class DatasetImpl extends OWLThingImpl implements Dataset {
  public static final String TypeIRI = "http://rdfs.org/ns/void#Dataset";

  protected DatasetImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Dataset make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new DatasetImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Dataset.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Dataset.class,false);
          if(toRet == null) {
            toRet = new DatasetImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Dataset)) {
          throw new RuntimeException("Instance of org.rdfs.ns._void.domain.impl.DatasetImpl expected");
        }
      }
      return (Dataset)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("dc:hasVersion");
  }

  public String getDescription() {
    return this.getStringLit("dc:description",true);
  }

  public void setDescription(String val) {
    this.setStringLit("dc:description",val);
  }

  public String getDataDump() {
    return this.getExternalRef("void:dataDump",true);
  }

  public void setDataDump(String val) {
    this.setExternalRef("void:dataDump",val);
  }

  public String getWaiver() {
    return this.getStringLit("wv:waiver",true);
  }

  public void setWaiver(String val) {
    this.setStringLit("wv:waiver",val);
  }

  public void remLanguage(String val) {
    this.remStringLit("dc:language",val,true);
  }

  public List<? extends String> getAllLanguage() {
    return this.getStringLitSet("dc:language",true);
  }

  public void addLanguage(String val) {
    this.addStringLit("dc:language",val);
  }

  public String getUriRegexPattern() {
    return this.getStringLit("void:uriRegexPattern",true);
  }

  public void setUriRegexPattern(String val) {
    this.setStringLit("void:uriRegexPattern",val);
  }

  public String getUriLookupEndpoint() {
    return this.getExternalRef("void:uriLookupEndpoint",true);
  }

  public void setUriLookupEndpoint(String val) {
    this.setExternalRef("void:uriLookupEndpoint",val);
  }

  public String getSparqlEndpoint() {
    return this.getExternalRef("void:sparqlEndpoint",true);
  }

  public void setSparqlEndpoint(String val) {
    this.setExternalRef("void:sparqlEndpoint",val);
  }

  public String getUriSpace() {
    return this.getExternalRef("void:uriSpace",true);
  }

  public void setUriSpace(String val) {
    this.setExternalRef("void:uriSpace",val);
  }

  public String getHasVersion() {
    return this.getStringLit("dc:hasVersion",false);
  }

  public void setHasVersion(String val) {
    this.setStringLit("dc:hasVersion",val);
  }

  public String getTitle() {
    return this.getStringLit("dc:title",true);
  }

  public void setTitle(String val) {
    this.setStringLit("dc:title",val);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }

  public String getLabel() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#label",val);
  }

  public String getNorms() {
    return this.getExternalRef("wv:norms",true);
  }

  public void setNorms(String val) {
    this.setExternalRef("wv:norms",val);
  }

  public String getLicense() {
    return this.getExternalRef("dc:license",true);
  }

  public void setLicense(String val) {
    this.setExternalRef("dc:license",val);
  }
}
