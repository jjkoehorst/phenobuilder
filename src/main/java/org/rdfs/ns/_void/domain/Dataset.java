package org.rdfs.ns._void.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://rdfs.org/ns/void# ontology
 */
public interface Dataset extends OWLThing {
  String getDescription();

  void setDescription(String val);

  String getDataDump();

  void setDataDump(String val);

  String getWaiver();

  void setWaiver(String val);

  void remLanguage(String val);

  List<? extends String> getAllLanguage();

  void addLanguage(String val);

  String getUriRegexPattern();

  void setUriRegexPattern(String val);

  String getUriLookupEndpoint();

  void setUriLookupEndpoint(String val);

  String getSparqlEndpoint();

  void setSparqlEndpoint(String val);

  String getUriSpace();

  void setUriSpace(String val);

  String getHasVersion();

  void setHasVersion(String val);

  String getTitle();

  void setTitle(String val);

  String getComment();

  void setComment(String val);

  String getLabel();

  void setLabel(String val);

  String getNorms();

  void setNorms(String val);

  String getLicense();

  void setLicense(String val);
}
