package nl.systemsbiology.pheno.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://systemsbiology.nl/pheno/ ontology
 */
public interface PhenotypeInformation extends OWLThing {
}
