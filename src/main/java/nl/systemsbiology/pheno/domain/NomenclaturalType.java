package nl.systemsbiology.pheno.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://systemsbiology.nl/pheno/ ontology
 */
public enum NomenclaturalType implements EnumClass {
  Paralectotype("http://gbol.life/0.1/Paralectotype",new NomenclaturalType[]{}),

  ReferenceStrain("http://gbol.life/0.1/ReferenceStrain",new NomenclaturalType[]{}),

  Paratype("http://gbol.life/0.1/Paratype",new NomenclaturalType[]{}),

  Syntype("http://gbol.life/0.1/Syntype",new NomenclaturalType[]{}),

  Neotype("http://gbol.life/0.1/Neotype",new NomenclaturalType[]{}),

  ExType("http://gbol.life/0.1/ExType",new NomenclaturalType[]{}),

  Isosyntype("http://gbol.life/0.1/Isosyntype",new NomenclaturalType[]{}),

  Allotype("http://gbol.life/0.1/Allotype",new NomenclaturalType[]{}),

  Lectotype("http://gbol.life/0.1/Lectotype",new NomenclaturalType[]{}),

  TypeStrain("http://gbol.life/0.1/TypeStrain",new NomenclaturalType[]{}),

  Isotype("http://gbol.life/0.1/Isotype",new NomenclaturalType[]{}),

  Hapanotype("http://gbol.life/0.1/Hapanotype",new NomenclaturalType[]{}),

  Epitype("http://gbol.life/0.1/Epitype",new NomenclaturalType[]{}),

  Holotype("http://gbol.life/0.1/Holotype",new NomenclaturalType[]{}),

  TypeMaterial("http://gbol.life/0.1/TypeMaterial",new NomenclaturalType[]{}),

  NeotypeStrain("http://gbol.life/0.1/NeotypeStrain",new NomenclaturalType[]{});

  private NomenclaturalType[] parents;

  private String iri;

  private NomenclaturalType(String iri, NomenclaturalType[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static NomenclaturalType make(String iri) {
    for(NomenclaturalType item : NomenclaturalType.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
