package nl.systemsbiology.pheno.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://systemsbiology.nl/pheno/ ontology
 */
public enum Rank implements EnumClass {
  RankSuperClass("http://gbol.life/0.1/RankSuperClass",new Rank[]{}),

  RankLevel28("http://gbol.life/0.1/RankLevel28",new Rank[]{}),

  RankLevel27("http://gbol.life/0.1/RankLevel27",new Rank[]{}),

  RankLevel26("http://gbol.life/0.1/RankLevel26",new Rank[]{}),

  RankTribe("http://gbol.life/0.1/RankTribe",new Rank[]{}),

  RankLevel25("http://gbol.life/0.1/RankLevel25",new Rank[]{}),

  RankKingdom("http://gbol.life/0.1/RankKingdom",new Rank[]{}),

  RankLevel20("http://gbol.life/0.1/RankLevel20",new Rank[]{}),

  RankLevel24("http://gbol.life/0.1/RankLevel24",new Rank[]{}),

  RankLevel23("http://gbol.life/0.1/RankLevel23",new Rank[]{}),

  RankLevel22("http://gbol.life/0.1/RankLevel22",new Rank[]{}),

  RankLevel21("http://gbol.life/0.1/RankLevel21",new Rank[]{}),

  RankOrder("http://gbol.life/0.1/RankOrder",new Rank[]{}),

  RankSubKingdom("http://gbol.life/0.1/RankSubKingdom",new Rank[]{}),

  RankSuperOrder("http://gbol.life/0.1/RankSuperOrder",new Rank[]{}),

  RankSpeciesSubGroup("http://gbol.life/0.1/RankSpeciesSubGroup",new Rank[]{}),

  RankSubSpecies("http://gbol.life/0.1/RankSubSpecies",new Rank[]{}),

  RankSubClass("http://gbol.life/0.1/RankSubClass",new Rank[]{}),

  RankSpecies("http://gbol.life/0.1/RankSpecies",new Rank[]{}),

  RankLevel9("http://gbol.life/0.1/RankLevel9",new Rank[]{}),

  RankLevel6("http://gbol.life/0.1/RankLevel6",new Rank[]{}),

  RankSubOrder("http://gbol.life/0.1/RankSubOrder",new Rank[]{}),

  RankLevel5("http://gbol.life/0.1/RankLevel5",new Rank[]{}),

  RankLevel8("http://gbol.life/0.1/RankLevel8",new Rank[]{}),

  RankVarietas("http://gbol.life/0.1/RankVarietas",new Rank[]{}),

  RankLevel7("http://gbol.life/0.1/RankLevel7",new Rank[]{}),

  RankLevel2("http://gbol.life/0.1/RankLevel2",new Rank[]{}),

  RankLevel1("http://gbol.life/0.1/RankLevel1",new Rank[]{}),

  RankLevel4("http://gbol.life/0.1/RankLevel4",new Rank[]{}),

  RankClass("http://gbol.life/0.1/RankClass",new Rank[]{}),

  RankLevel3("http://gbol.life/0.1/RankLevel3",new Rank[]{}),

  RankParvOrder("http://gbol.life/0.1/RankParvOrder",new Rank[]{}),

  RankForma("http://gbol.life/0.1/RankForma",new Rank[]{}),

  RankInfraOrder("http://gbol.life/0.1/RankInfraOrder",new Rank[]{}),

  RankSuperPhylum("http://gbol.life/0.1/RankSuperPhylum",new Rank[]{}),

  RankSpeciesGroup("http://gbol.life/0.1/RankSpeciesGroup",new Rank[]{}),

  RankSubTribe("http://gbol.life/0.1/RankSubTribe",new Rank[]{}),

  RankInfraClass("http://gbol.life/0.1/RankInfraClass",new Rank[]{}),

  RankFamily("http://gbol.life/0.1/RankFamily",new Rank[]{}),

  RankLevel17("http://gbol.life/0.1/RankLevel17",new Rank[]{}),

  RankLevel16("http://gbol.life/0.1/RankLevel16",new Rank[]{}),

  RankLevel15("http://gbol.life/0.1/RankLevel15",new Rank[]{}),

  RankLevel14("http://gbol.life/0.1/RankLevel14",new Rank[]{}),

  RankSuperFamily("http://gbol.life/0.1/RankSuperFamily",new Rank[]{}),

  RankSubGenus("http://gbol.life/0.1/RankSubGenus",new Rank[]{}),

  RankLevel19("http://gbol.life/0.1/RankLevel19",new Rank[]{}),

  RankLevel18("http://gbol.life/0.1/RankLevel18",new Rank[]{}),

  RankSubFamily("http://gbol.life/0.1/RankSubFamily",new Rank[]{}),

  RankLevel13("http://gbol.life/0.1/RankLevel13",new Rank[]{}),

  RankLevel12("http://gbol.life/0.1/RankLevel12",new Rank[]{}),

  RankLevel11("http://gbol.life/0.1/RankLevel11",new Rank[]{}),

  RankPhylum("http://gbol.life/0.1/RankPhylum",new Rank[]{}),

  RankGenus("http://gbol.life/0.1/RankGenus",new Rank[]{}),

  RankLevel10("http://gbol.life/0.1/RankLevel10",new Rank[]{}),

  RankSubPhylum("http://gbol.life/0.1/RankSubPhylum",new Rank[]{}),

  RankSuperKingdom("http://gbol.life/0.1/RankSuperKingdom",new Rank[]{});

  private Rank[] parents;

  private String iri;

  private Rank(String iri, Rank[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static Rank make(String iri) {
    for(Rank item : Rank.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
