package nl.systemsbiology.pheno.domain.impl;

import java.lang.String;
import nl.systemsbiology.pheno.domain.CellInformation;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://systemsbiology.nl/pheno/ ontology
 */
public class CellInformationImpl extends PhenotypeInformationImpl implements CellInformation {
  public static final String TypeIRI = "http://systemsbiology.nl/pheno/CellInformation";

  protected CellInformationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static CellInformation make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new CellInformationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,CellInformation.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,CellInformation.class,false);
          if(toRet == null) {
            toRet = new CellInformationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof CellInformation)) {
          throw new RuntimeException("Instance of nl.systemsbiology.pheno.domain.impl.CellInformationImpl expected");
        }
      }
      return (CellInformation)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getGram() {
    return this.getStringLit("http://systemsbiology.nl/pheno/gram",true);
  }

  public void setGram(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/gram",val);
  }

  public String getShape() {
    return this.getStringLit("http://systemsbiology.nl/pheno/shape",true);
  }

  public void setShape(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/shape",val);
  }

  public String getColor() {
    return this.getStringLit("http://systemsbiology.nl/pheno/color",true);
  }

  public void setColor(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/color",val);
  }

  public String getDiameter() {
    return this.getStringLit("http://systemsbiology.nl/pheno/diameter",true);
  }

  public void setDiameter(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/diameter",val);
  }
}
