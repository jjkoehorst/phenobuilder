package nl.systemsbiology.pheno.domain.impl;

import java.lang.String;
import nl.systemsbiology.pheno.domain.PhenotypeInformation;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://systemsbiology.nl/pheno/ ontology
 */
public class PhenotypeInformationImpl extends OWLThingImpl implements PhenotypeInformation {
  public static final String TypeIRI = "http://systemsbiology.nl/pheno/PhenotypeInformation";

  protected PhenotypeInformationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static PhenotypeInformation make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PhenotypeInformationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,PhenotypeInformation.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,PhenotypeInformation.class,false);
          if(toRet == null) {
            toRet = new PhenotypeInformationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof PhenotypeInformation)) {
          throw new RuntimeException("Instance of nl.systemsbiology.pheno.domain.impl.PhenotypeInformationImpl expected");
        }
      }
      return (PhenotypeInformation)toRet;
    }
  }

  public void validate() {
    super.validate();
  }
}
