package nl.systemsbiology.pheno.domain.impl;

import java.lang.String;
import nl.systemsbiology.pheno.domain.Environment;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://systemsbiology.nl/pheno/ ontology
 */
public class EnvironmentImpl extends PhenotypeInformationImpl implements Environment {
  public static final String TypeIRI = "http://systemsbiology.nl/pheno/Environment";

  protected EnvironmentImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Environment make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new EnvironmentImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Environment.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Environment.class,false);
          if(toRet == null) {
            toRet = new EnvironmentImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Environment)) {
          throw new RuntimeException("Instance of nl.systemsbiology.pheno.domain.impl.EnvironmentImpl expected");
        }
      }
      return (Environment)toRet;
    }
  }

  public void validate() {
    super.validate();
  }

  public String getSalinityName() {
    return this.getStringLit("http://systemsbiology.nl/pheno/salinityName",true);
  }

  public void setSalinityName(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/salinityName",val);
  }

  public String getPhName() {
    return this.getStringLit("http://systemsbiology.nl/pheno/phName",true);
  }

  public void setPhName(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/phName",val);
  }

  public String getOxygenRequirementName() {
    return this.getStringLit("http://systemsbiology.nl/pheno/oxygenRequirementName",true);
  }

  public void setOxygenRequirementName(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/oxygenRequirementName",val);
  }

  public String getPressureName() {
    return this.getStringLit("http://systemsbiology.nl/pheno/pressureName",true);
  }

  public void setPressureName(String val) {
    this.setStringLit("http://systemsbiology.nl/pheno/pressureName",val);
  }
}
