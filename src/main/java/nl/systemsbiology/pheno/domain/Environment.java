package nl.systemsbiology.pheno.domain;

import java.lang.String;

/**
 * Code generated from http://systemsbiology.nl/pheno/ ontology
 */
public interface Environment extends PhenotypeInformation {
  String getSalinityName();

  void setSalinityName(String val);

  String getPhName();

  void setPhName(String val);

  String getOxygenRequirementName();

  void setOxygenRequirementName(String val);

  String getPressureName();

  void setPressureName(String val);
}
