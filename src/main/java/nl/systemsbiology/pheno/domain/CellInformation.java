package nl.systemsbiology.pheno.domain;

import java.lang.String;

/**
 * Code generated from http://systemsbiology.nl/pheno/ ontology
 */
public interface CellInformation extends PhenotypeInformation {
  String getGram();

  void setGram(String val);

  String getShape();

  void setShape(String val);

  String getColor();

  void setColor(String val);

  String getDiameter();

  void setDiameter(String val);
}
