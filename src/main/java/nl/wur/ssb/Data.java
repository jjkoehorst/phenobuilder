package nl.wur.ssb;

import life.gbol.domain.Database;
import life.gbol.domain.Organism;
import life.gbol.domain.Taxon;
import life.gbol.domain.TaxonomyRef;
import nl.systemsbiology.pheno.domain.CellInformation;
import nl.systemsbiology.pheno.domain.Environment;
import nl.systemsbiology.pheno.domain.PhenotypeInformation;
import nl.systemsbiology.pheno.domain.Rank;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

import java.io.File;
import java.util.Scanner;

public class Data {
    public static void parser() throws Exception {
        goldParser(new File("./Naths_10_GOLD_data.csv"));
    }

    private static void goldParser(File gold) throws Exception {
        // Memory triple store
        Domain domain = new Domain("");

        Scanner scanner = new Scanner(gold);
        // Skipping the header
        scanner.nextLine();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            System.err.println(line);
            String[] lineSplit = line.split(";");

            String GOLD_Organism_ID = lineSplit[0];
            if (GOLD_Organism_ID.length() > 0) {
                Organism organism = domain.make(life.gbol.domain.Organism.class, "http://example.com/organism/" + GOLD_Organism_ID);
                CellInformation cellInformation = domain.make(CellInformation.class, organism.getResource().getURI() + "/cellInformation");
                Environment environmentInformation = domain.make(Environment.class,organism.getResource().getURI() + "/environmentInformation");

                String Organism_Name = lineSplit[1];
                if (Organism_Name.length() > 0) {
                    organism.setScientificName(Organism_Name);
                }

                String Other_Names = lineSplit[2];
                if (Other_Names.length() > 0) {
                    organism.setAlternativeNames(Other_Names);
                }

                String Common_Name = lineSplit[3];
                if (Common_Name.length() > 0) {
                    organism.setCommonName(Common_Name);
                }

                String NCBI_Taxonomy_ID = lineSplit[4];
                if (NCBI_Taxonomy_ID.length() > 0) {
                    TaxonomyRef taxonomy = domain.make(TaxonomyRef.class, "http://example.com/taxonomy/" + NCBI_Taxonomy_ID);
                    taxonomy.setAccession(NCBI_Taxonomy_ID);
                    Database database = domain.make(life.gbol.domain.Database.class, "http://example.com/database/ncbitaxonomy");
                    database.setComment("NCBI Taxonomy database");
                    database.setVersion("0");
                    database.setHasVersion("0");
                    database.setId(NCBI_Taxonomy_ID);
                    taxonomy.setDb(database);
                    organism.setTaxonomy(taxonomy);
                }

                if (lineSplit[5].length() > 0) {
                    organism.addTaxonomyLineage(setRank(domain, Rank.RankKingdom, lineSplit[5]));
                }

                if (lineSplit[6].length() > 0) {
                    organism.addTaxonomyLineage(setRank(domain, Rank.RankPhylum, lineSplit[6]));
            }
                if (lineSplit[7].length() >0) {
                    organism.addTaxonomyLineage(setRank(domain, Rank.RankGenus, lineSplit[7]));
                }

                String Genus_Synonyms = lineSplit[8];
                if (lineSplit[9].length() > 0) {
                    organism.addTaxonomyLineage(setRank(domain, Rank.RankSpecies, lineSplit[9]));
                }
                String Species_Synonyms = lineSplit[10];

                String Serovar_Cultivar = lineSplit[11];
                String Strain = lineSplit[12];
                if (lineSplit[13].length() > 0) {
                    organism.addTaxonomyLineage(setRank(domain, Rank.RankSubSpecies, lineSplit[13]));
                }

                String Type_Strain = lineSplit[14];
                if (Type_Strain.length() >0) {
                    System.err.println("type strain " + Type_Strain);
                }
                String Culture_Collection_ID = lineSplit[15];
                String Organism_Type = lineSplit[16];
                String Cultured = lineSplit[17];
                String Culture_Type = lineSplit[18];
                String Comments = lineSplit[19];
                String Biosafety_Level = lineSplit[20];
                String Taxon_DOI = lineSplit[21];
                String Exemplar_DOI = lineSplit[22];
                String Exemplar_Name = lineSplit[23];
                String Genbank_16S_ID = lineSplit[24];
                String Is_Public = lineSplit[25];
                String Related_Sequencing_Projects = lineSplit[26];
                String Related_Analysis_Projects = lineSplit[27];
                String NCBI_Organism_Name = lineSplit[28];
                String NCBI_Tax_ID = lineSplit[29];
                String NCBI_Superkingdom = lineSplit[30];
                String NCBI_Kingdom = lineSplit[31];
                String NCBI_Phylum = lineSplit[32];
                String NCBI_Class = lineSplit[33];
                String NCBI_Order = lineSplit[34];
                String NCBI_Family = lineSplit[35];
                String NCBI_Genus = lineSplit[36];
                String NCBI_Species = lineSplit[37];
                String Cell_Diameter = lineSplit[38];
                String Cell_Shape = lineSplit[39];


                String Color = lineSplit[40];
                if (Color.length() > 0) {
                    cellInformation.setColor(Color);
                }
                String Gram_Stain = lineSplit[41];
                if (Gram_Stain.length() > 0) {
                    cellInformation.setGram(Gram_Stain);
                }
                String Motility = lineSplit[42];
                String Oxygen_Requirement = lineSplit[43];
                if (Oxygen_Requirement.length() > 0) {
                    environmentInformation.setOxygenRequirementName(Oxygen_Requirement);
                }
                String pH = lineSplit[44];
                if (pH.length() > 0) {
                    environmentInformation.setPhName(pH);
                }
                String Salinity = lineSplit[45];
                if (Salinity.length() > 0) {
                    environmentInformation.setSalinityName(Salinity);
                }
                String Pressure = lineSplit[46];
                if (Pressure.length() > 0) {
                    environmentInformation.setPressureName(Pressure);
                }
                String Sporulation = lineSplit[47];
                if (Sporulation.length() > 0) {

                }
                String Carbon_Source = lineSplit[0];
                String Biotic_Relationships = lineSplit[0];
                String Temperature_Range = lineSplit[0];
                String Growth_Temperature = lineSplit[0];
                String Cell_Length = lineSplit[0];
                String Commercial_Strain = lineSplit[0];
                String Commercial_Strain_Comments = lineSplit[0];
                String Cell_Arrangements = lineSplit[0];
                String Diseases = lineSplit[0];
                String Habitats = lineSplit[0];
                String Metabolism = lineSplit[0];
                String Phenotypes = lineSplit[0];
                String Energy_Sources = lineSplit[0];
                String Collection_Date = lineSplit[0];
                String Sample_Isolation_Publication = lineSplit[0];
                String Sample_Collection_Site = lineSplit[0];
                String Isolation_Country = lineSplit[0];
                String Sample_Isolation_Comments = lineSplit[0];
                String Sample_Collection_Method = lineSplit[0];
                String Contact_Name = lineSplit[0];
                String Contact_Email = lineSplit[0];
                String Isolation_Host_Name = lineSplit[0];
                String Host_Taxonomy_ID = lineSplit[0];
                String Ecosystem = lineSplit[0];
                String Ecosystem_Category = lineSplit[0];
                String Ecosystem_Type = lineSplit[0];
                String Ecosystem_Subtype = lineSplit[0];
                String Specific_Ecosystem = lineSplit[0];
                String Ecosystem_Path_Id = lineSplit[0];
                String Geographic_Location = lineSplit[0];
                String Latitude = lineSplit[0];
                String Longitude = lineSplit[0];
                String LatLong_Information = lineSplit[0];
                String Geographical_Map = lineSplit[0];
                String Broad_scale_environmental_context = lineSplit[0];
                String Local_environmental_context = lineSplit[0];
                String Environmental_medium = lineSplit[0];
                String Altitude = lineSplit[0];
                String Elevation = lineSplit[0];
                String Depth = lineSplit[0];
                String Subsurface_Depth = lineSplit[0];
                String Sample_Collection_Temperature = lineSplit[0];
                String Chlorophyll_Concentration = lineSplit[0];
                String Nitrate_Concentration = lineSplit[0];
                String Oxygen_Concentration = lineSplit[0];
                String Salinity_Concentration = lineSplit[0];
                String Methane_Concentration_mmol = lineSplit[0];
                String Sample_Conductivity = lineSplit[0];
                String Growth_Conditions = lineSplit[0];
                String Bicarbonate_Concentration_mM = lineSplit[0];
                String Soluble_Iron_Concentration_uM = lineSplit[0];
                String Hydrogen_Sulfide_Concentration_mM = lineSplit[0];
                String Hydrogen_Sulfide_Presence = lineSplit[0];
                String Irradiance = lineSplit[0];
                String ProPortal_Ocean = lineSplit[0];
                String ProPortal_Isolation = lineSplit[0];
                String ProPortal_Station = lineSplit[0];
                String ProPortal_WOA_Temperature = lineSplit[0];
                String ProPortal_WOA_Salinty = lineSplit[0];
                String ProPortal_WOA_Oxygen = lineSplit[0];
                String ProPortal_WOA_Silicate = lineSplit[0];
                String ProPortal_WOA_Phosphate = lineSplit[0];
                String ProPortal_WOA_Nitrate = lineSplit[0];

                organism.addPhenotypeInformation(cellInformation);
            }
        }

        domain.save("result.ttl");
    }

    private static Taxon setRank(Domain domain, Rank rank, String lineageName) {
        Taxon lineageDomain = domain.make(life.gbol.domain.Taxon.class,"http://example.com/lineage/" + lineageName.replaceAll(" ","_"));
        lineageDomain.setTaxonName(lineageName);
        lineageDomain.setTaxonRank(Rank.RankKingdom);
        return lineageDomain;
    }
}
